# 问题描述

我在 widget 文件夹里面建了一个只使用 `<src href=".js">` 标签的 widget

```r
testit = function(option, ...){

  dep = htmlDependency(
          name = "testit",
          src = c(href = "https://code.jquery.com"),
          script = "jquery.min.js",
          version = "0.0.1"
        )

  return(
    htmlwidgets::createWidget(
      name = "testit",
      x    = list(option = option),
      dependencies = dep,
      package = "testit"
    )
  )

}
```

就是 `htmlDependency(src = c(file=filepath, href=url))` 里，没有设置 `file=filepath`，这个选项。

不设置  `file=filepath` 是想尝试减小生成网页的体积。

似乎这会触发 bookdown 构建网站的时候，出现

```
path for html_dependency not provided
```

可以用 book 文件夹的 bookdown 项目重复这个错误。

我搜了一个 GitHub 的代码，这应该是出自 rmarkdown 的报错

https://github.com/rstudio/rmarkdown/blob/444d7ae381a84ae32bf32cdfbb64882b799b1596/R/html_dependencies.R#L215-L216

```r
validate_html_dependency <- function(list) {
  ...

  if (is.null(list$src$file))
    stop("path for html_dependency not provided", call. = FALSE)
  file <- list$src$file
  
  ...
}
```
